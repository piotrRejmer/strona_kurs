<?php

namespace Common\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Security;
use Sensio\Bundle\FrameworkExtraBundle\Request;


class LoginController extends Controller
{
    /**
     * @Route(
     *     "/login",
     *     name= "login"
     * )
     */
    public function loginAction(Request $request)
    {
        $Session = $this->get('session');

        if($request->attributes->has(Security::AUTHENTICATION_ERROR)){
         //  TODO: pozmienialo sie w stosunku do filmiku z EduWeb zrobic z sensem
        }
        return $this->render('CommonUserBundle:Login:login.html.twig');
    }


}
