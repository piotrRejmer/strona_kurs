<?php
/**
 * Created by PhpStorm.
 * User: rejki
 * Date: 10.02.16
 * Time: 11:52
 */

namespace Air\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;
class TagRepository extends EntityRepository
{
   public function getTagsListOcc(){
       $qb = $this->createQueryBuilder('t')
           ->select('t.slug, t.name, COUNT(p) as occ' )
           ->leftJoin('t.posts', 'p')
           ->groupBy('t.name');

       return $qb->getQuery()->getArrayResult();
   }



}