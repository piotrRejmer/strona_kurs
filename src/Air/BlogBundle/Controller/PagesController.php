<?php

namespace Air\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PagesController extends Controller
{
    /**
     * @Route(
     *     "/about",
     *     name= "blog_about"
     * )
     */
    public function aboutAction()
    {
        return $this->render('AirBlogBundle:Pages:about.html.twig');
    }

    /**
     * @Route(
     *     "/contact",
     *     name= "blog_contact"
     * )
     */
    public function contactAction()
    {
        return $this->render('AirBlogBundle:Pages:contact.html.twig');
    }

    /**
     * @Route(
     *     "/login",
     *     name= "login"
     * )
     */
    public function loginAction()
    {
        $Session = $this->get('session');

        if($request->attributes->has(Security::AUTHENTICATION_ERROR)){
            //  TODO: pozmienialo sie w stosunku do filmiku z EduWeb zrobic z sensem
        }
        return $this->render('AirBlogBundle:Pages:contact.html.twig');
    }
}
