<?php

namespace Air\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class PostsController extends Controller
{
    protected $itemsLimit = 3;

    /**
     * @Route(
     *     "/{page}",
     *     name= "blog_index",
     *     defaults = {"page" =1},
     *     requirements = {"page" = "\d+"}
     *     )
     */
    public function indexAction($page)
    {


        // dzieki createQueryBuldier ilosc zapytan zostala zmniejszona w bazie, refaktoryzacja - getPaginatedPosts aby nie powtarzac paginatora
        $pagination = $this->getPaginatedPosts(array(
            'status' => 'published',
            'orderBy' => 'p.publishedDate',
            'orderDir' => 'DESC'
        ),$page);

        return $this->render('AirBlogBundle:Posts:postList.html.twig',array(
            'pagination' => $pagination,
            'listTitle' => 'Najnowsze wpisy'
        ));
    }

    /**
     * @Route(
     *     "/search/{page}",
     *     name= "blog_search",
     *     defaults = {"page" =1},
     *     requirements = {"page" = "\d+"}
     *     )
     */
    public function searchAction(Request $request, $page)
    {
        $searchParam = $request->query->get('search');

        // dzieki createQueryBuldier ilosc zapytan zostala zmniejszona w bazie, refaktoryzacja - getPaginatedPosts aby nie powtarzac paginatora
        $pagination = $this->getPaginatedPosts(array(
            'status' => 'published',
            'orderBy' => 'p.publishedDate',
            'orderDir' => 'DESC',
            'search' => $searchParam
        ),$page);

        return $this->render('AirBlogBundle:Posts:postList.html.twig',array(
            'pagination' => $pagination,
            'listTitle' => sprintf('Wyniki wyszukiwania frazy "%s"',$searchParam),
            'searchParam' => $searchParam
        ));
    }

    /**
     * @Route(
     *     "/{slug}",
     *     name = "blog_post"
     * )
     */
    public function postAction($slug)
    {
        $PostRepo = $this->getDoctrine()->getRepository('AirBlogBundle:Post');

        $Post = $PostRepo->getPublishedPost($slug);

        if(null === $Post){
            throw $this->createNotFoundException('Post nie został znaleziony');
        }
        return $this->render('AirBlogBundle:Posts:post.html.twig',array(
            'post' => $Post
        ));
    }

    /**
     * @Route(
     *     "/category/{slug}/{page}",
     *     name= "blog_category",
     *     defaults = {"page" =1},
     *     requirements = {"page" = "\d+"}
     *     )
     */
    public function categoryAction($slug, $page)
    {

        $pagination = $this->getPaginatedPosts(array(
            'status' => 'published',
            'orderBy' => 'p.publishedDate',
            'orderDir' => 'DESC',
            'categorySlug'=>$slug
        ),$page);

        $CategoryRepo = $this->getDoctrine()->getRepository('AirBlogBundle:Category');
        $Category = $CategoryRepo->findOneBySlug($slug);


        return $this->render('AirBlogBundle:Posts:postList.html.twig',array(
            'pagination' => $pagination,
            'listTitle' => sprintf('Wpisy w kategorii "%s"',$Category->getName())
        ));
    }

    /**
     * @Route(
     *     "/tag/{slug}/{page}",
     *     name= "blog_tag",
     *     defaults = {"page" =1},
     *     requirements = {"page" = "\d+"}
     *     )
     */
    public function tagAction($slug, $page)
    {
        $TagRepo = $this->getDoctrine()->getRepository('AirBlogBundle:Tag');
        $Tag = $TagRepo->findOneBySlug($slug);

        $pagination = $this->getPaginatedPosts(array(
            'status' => 'published',
            'orderBy' => 'p.publishedDate',
            'orderDir' => 'DESC',
            'tagSlug'=>$slug
        ),$page);

        return $this->render('AirBlogBundle:Posts:postList.html.twig',array(
            'pagination' => $pagination,
            'listTitle' => sprintf('Wpisy z tagiem "%s"',$Tag->getName())
        ));
    }

    protected function getPaginatedPosts(array $params = array(), $page){

        $PostRepo = $this->getDoctrine()->getRepository('AirBlogBundle:Post');
        $qb = $PostRepo->getQueryBuilder($params);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($qb,$page,$this->itemsLimit);

        return $pagination;
    }
}
